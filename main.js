//==================
//=== PIZZÓMETRO ===
//==================
// TODO: Añadir la diferencia de área entre las pizzas (podría ser con el hover);
// TODO: Avisar cuando creas una pizza igual que otra existente;
// TODO: No limitar a una la pizza más barata;
// TODO: Añadir el número de comensales;
// TODO: Definir una función para que compruebe que los valores introducidos
// en crearPizza sean números positivos;

'use strict';

// Definimos un constructor para el objeto "Pizza":
function Pizza(diametro, precio, porciones) {
    var self = this;
    self.diametro = __numero(diametro.toString().replace(',','.'));
    self.precio = __numero(precio.toString().replace(',','.'));
    self.porciones = __numero(porciones); // 4, 8, 16, ...
    self.masBarata = false;

    //=== MÉTODOS:
    // Asignamos un número:
    self.numero = function() {
        var numero;
        if(listaPizzas.length === 0) {
            numero = 1;
        } else {
            numero = listaPizzas[(listaPizzas.length - 1)].numero + 1
        }
        return numero;
    };
    // Calculamos el radio:
    self.radio = function() {
        diametro = self.diametro;
        var radio = diametro/2;
        return radio;
    };
    // Calculamos el área:
    self.area = function() {
        diametro = self.diametro;
        var radio = self.radio(diametro);
        var radioCuadrado = Math.pow(radio, 2);
        var area = Math.PI * radioCuadrado;
        return area;
    };
    // Calculamos el precio por centímetro:
    self.precioCentimetro = function() {
        precio = self.precio;
        diametro = self.diametro;
        var area = self.area(diametro);
        var precioCentimetro = precio/area; // Podría ser así self.area(diametro);
        return precioCentimetro;
    };
    // Calculamos el precio por porción:
    self.precioPorcion = function() {
        precio = self.precio;
        porciones = self.porciones;
        var precioPorcion = precio/porciones;
        return precioPorcion;
    };
}


//=== UTILIDADES ===
// Redondeamos el valor a dos decimales:
function __redondeo(valor) {
    var valor_redondeado = Math.round(valor * 100)/100;
    return valor_redondeado;
}

function __numero(valor) {
    var numero = parseFloat(valor);
    return numero;
}

function __minimo(array) {
    return Math.min.apply(Math, array);
}

Array.min = function( array ){
    return Math.min.apply( Math, array );
};


//=== FUNCIONALIDAD WEB ===
var boton = document.querySelector('button'),
    listaPizzas = [],
    pizzaMasBarata,
    precioMinimo
;

boton.addEventListener('click', servicioCompleto);

function servicioCompleto() {
    crearPizza();
    servirUltimaPizza();
    seleccionarPizza();
}

function crearPizza() {
    // parseFloat(elemento.value) || 0;
    var _diametro = __numero(document.querySelector('#diametro').value),
        _precio = __numero(document.querySelector('#precio').value),
        _porciones = __numero(document.querySelector('#porciones').value),
        _nuevaPizza = new Pizza(_diametro, _precio, _porciones),
        _numero = _nuevaPizza.numero(),
        _area = __redondeo(_nuevaPizza.area()),
        _precioCentimetroRedondeado = __redondeo(_nuevaPizza.precioCentimetro()),
        _precioPorcionRedondeado = __redondeo(_nuevaPizza.precioPorcion())
    ;

    _nuevaPizza.numero = _numero;
    _nuevaPizza.diametro = _diametro;
    _nuevaPizza.porciones = _porciones;
    _nuevaPizza.precio = _precio;
    _nuevaPizza.area = _area;
    _nuevaPizza.precioCentimetro = _precioCentimetroRedondeado;
    _nuevaPizza.precioPorcion = _precioPorcionRedondeado;

    listaPizzas.push(_nuevaPizza);
    console.log(listaPizzas);
}

function servirUltimaPizza() {
    var _destino = document.querySelector('#listado'),
        i = listaPizzas.length - 1,
        _pizzaNumero = __numero(listaPizzas[i].numero),
        _diametro = __numero(__redondeo(listaPizzas[i].diametro)),
        _porciones = __numero(listaPizzas[i].porciones),
        _precio = __numero(__redondeo(listaPizzas[i].precio)),
        _area = __numero(__redondeo(listaPizzas[i].area)),
        _precioCentimetro = __numero(__redondeo(listaPizzas[i].precioCentimetro)),
        _precioPorcion = __numero(__redondeo(listaPizzas[i].precioPorcion))
    ;

    console.log('Servimos la pizza' + _pizzaNumero);

    _destino.innerHTML += (
        '<li id="pizza' + _pizzaNumero + '">' +
            '<strong>Pizza número ' + _pizzaNumero + '</strong><span class="eliminar">' + equis + '</span><br>' +//i
            'Diámetro: ' + _diametro + 'cm<br>' +
            'Precio: ' + _precio + '€<br>' +
            'Porciones: ' + _porciones + '<br>' +
            'Área: ' + _area + 'cm2<br>' +
            'Precio por centímetro: ' + _precioCentimetro + '€/cm<br>' +
            'Precio por porción: ' + _precioPorcion + '€/porción<br>' +
        '</li>'
    );
    buscarPrecioMinimo();
}

function seleccionarPizza() {
    var pizzaSeleccionada = document.querySelectorAll('#listado li span.eliminar');
    for (let i = 0; i < pizzaSeleccionada.length; i++) {
        pizzaSeleccionada[i].addEventListener('click', tirarPizza);
    }
}

function tirarPizza() {
    var _pizzaFuera = this.parentElement,
        _idPizzaFuera = _pizzaFuera.id,
        _listaPizzas = document.querySelectorAll('#listado li'),
        _posicionPizzaFuera = __numero((_idPizzaFuera.split('a')[1]))
    ;

    console.log('Tiramos la ' + _idPizzaFuera);
    for(let i = 0; i < listaPizzas.length; i++) {
        if((_posicionPizzaFuera != -1) && (listaPizzas[i].numero === _posicionPizzaFuera)) {
            if(listaPizzas[i].masBarata) {
                pizzaMasBarata = '';
                precioMinimo = '';
            }
            listaPizzas.splice(i, 1);
        }
    }
    _pizzaFuera.parentNode.removeChild(_pizzaFuera);
    console.log(listaPizzas);
    buscarPrecioMinimo();
}


function buscarPrecioMinimo() {
    var _pizzas = document.querySelectorAll('#listado li');

    for(let i = 0; i < listaPizzas.length; i++) {
        switch(precioMinimo) {
            case undefined:
                precioMinimo = listaPizzas[0].precioCentimetro;
                listaPizzas[0].masBarata = true;
                break;
            case '':
                precioMinimo = listaPizzas[0].precioCentimetro;
                listaPizzas[0].masBarata = true;
                break;
            default:
                if(listaPizzas[i].precioCentimetro < precioMinimo) {
                    pizzaMasBarata = i;
                    precioMinimo = listaPizzas[i].precioCentimetro;

                    function _encuentraBarataTrue(listaPizzas) {
                        return listaPizzas.masBarata === true;
                    }

                    listaPizzas.find(_encuentraBarataTrue).masBarata = false;
                    listaPizzas[i].masBarata = true;
                    console.log(pizzaMasBarata + 1, precioMinimo);
                }
                break;
        }
    }

    for(let i = 0; i < listaPizzas.length; i++) {
        let _estado = listaPizzas[i].masBarata;
        switch(_estado) {
            case true:
                console.log(i);
                _pizzas[i].className = 'pizzaMasBarata';
                break;
            default:
                _pizzas[i].className = '';
                break;
        }
    }
}

const equis = `<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="11.874px" height="11.502px" viewBox="0 0 11.874 11.502" enable-background="new 0 0 11.874 11.502" xml:space="preserve"><polygon points="11.874,1.188 10.478,0 5.981,4.5 5.937,4.54 5.892,4.5 1.396,0 0,1.188 4.583,5.751 0,10.314 1.396,11.502 5.892,7.002 5.937,6.961 5.981,7.002 10.478,11.502 11.874,10.314 7.29,5.751 "/></svg>`;
